Template.login.events({


    "submit .login-form": function (event) {
        event.preventDefault();

        var username = $('.login-username').val,
            password = $('.login-password').val;

        console.log("logging in");

        Meteor.loginWithPassword(username, password, function (error) {
            if (error) {
                Materialize.toast("Invalid username or password", 3000);
                return false;
            } else if (Meteor.user().username) {
                Router.go("chatroom");
            }
        });
        return false;
    }


});

Template.register.events({
    'submit .register-form': function () {
        event.preventDefault();

        var username = $('.register-username').val(),
            password = $('.register-password').val();

        Accounts.createUser({
            username: username,
            email: "",
            password: password,
            profile: null,

        });
    }
});

Template.chatroom.events({
    'submit .chat-message': function (event) {
        Router.go('chatroom');
        event.preventDefault();
        //var name = event.CurrentTarget.firstName.value;

        Posts.insert({
            name: name,
            createdAt: new Date(),
            username: Meteor.user().username,
        });

        return false;
    }
});




//Template.posts.onCreated(function () {
//  Meteor.subscribe('posts');
//});

//Template.post.events({
//  "submit .post-class": function (event) {
//    Router.go('chatroom');
//
//      Posts.insert({
//      name: name
//    });
//}
//});