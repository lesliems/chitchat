Router.route('/', function () {
    this.layout('ApplicationLayout');
    this.render('main');
    // this.render('Home', {
    // data: function () { return Items.findOne({_id: this.params._id}); }
}, ({
    name: "home"
}));

Router.route('/post/:_id', function () {
    this.layout('ApplicationLayout');
    this.render('post', function () {
        return Posts.findOne({
            _id: this.params._id
        });
    });
    // this.render('Home', {
    // data: function () { return Items.findOne({_id: this.params._id}); }
});


Router.route('/login', function () {
    this.layout('ApplicationLayout');
    this.render('login');

});

Router.route('/register', function () {
    this.layout('ApplicationLayout');
    this.render('register');

});


Router.route('/chatroom', function () {
    this.layout('ApplicationLayout');
    this.render('chatroom');

}, ({
    name: "chatroom"
}));